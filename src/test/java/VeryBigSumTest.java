import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class VeryBigSumTest {

    /*  Calculate and print the sum of the elements in an array, keeping in mind that some of those integers may be quite large.
        Function Description:
        Complete the aVeryBigSum function in the editor below. It must return the sum of all array elements.
        aVeryBigSum has the following parameter(s):
        ar: an array of integers .*/

    @Test
    void shouldSumFiveValues() {
        // given
        long[] arr = new long[]{1000000001, 1000000002, 1000000003, 1000000004, 1000000005};

        // when
        long result = VeryBigSum.aVeryBigSum(arr);

        // then
        assertThat(result, is(5000000015L));
    }

}