import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HourglassSumTest {
/*  Given a 6x6 2D Array
    1 1 1 0 0 0
    0 1 0 0 0 0
    1 1 1 0 0 0
    0 0 0 0 0 0
    0 0 0 0 0 0
    0 0 0 0 0 0
    We define an hourglass in array to be a subset of values with indices falling in this pattern
    made from 1's above.
    There are  hourglasses in arr, and an hourglass sum is the sum of an hourglass' values.
    Calculate the hourglass sum for every hourglass in arr, then print the maximum hourglass sum.
    Example:
        1 1 1 0 0 0
        0 1 0 0 0 0
        1 1 1 0 0 0
        0 0 2 4 4 0
        0 0 0 2 0 0
        0 0 1 2 4 0
        = 19
    */
    @Test
    void shouldCountHourglassOfOnes() {
        // given
        int[][] arr = new int[][]{
                { 1, 1, 1, 0, 0, 0},
                { 0, 1, 0, 0, 0, 0},
                { 1, 1, 1, 0, 0, 0},
                { 0, 0, 0, 0, 0, 0},
                { 0, 0, 0, 0, 0, 0},
                { 0, 0, 0, 0, 0, 0}
        };
        // when
        int result = HourglassSum.hourglassSum(arr);
        // then
        assertEquals(7, result);
    }

    @Test
    void shouldCountHourglass19() {
        // given
        int[][] arr = new int[][]{
                { 1, 1, 1, 0, 0, 0},
                { 0, 1, 0, 0, 0, 0},
                { 1, 1, 1, 0, 0, 0},
                { 0, 0, 2, 4, 4, 0},
                { 0, 0, 0, 2, 0, 0},
                { 0, 0, 1, 2, 4, 0}
        };
        // when
        int result = HourglassSum.hourglassSum(arr);
        // then
        assertEquals(19, result);
    }
}