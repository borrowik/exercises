import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class FirstFactorialTest{

    /*    Have the function FirstFactorial(num) take the num parameter being passed and return
    the factorial of it. For example: if num = 4, then your program should return (4 * 3 * 2 * 1) = 24.
    For the test cases, the range will be between 1 and 18 and the input will always be an integer.
    */

    @Test
    void shouldGetFactorialOfThree() {
        // given
        int input = 3;
        int expected = 6;

		// when + then
		assertThat(FirstFactorial.firstFactorial(input), is(expected));
    }

    @Test
    void shouldGetFactorialOfNine() {
        // given
        int input = 9;
        int expected = 362880;

        // when + then
        assertThat(FirstFactorial.firstFactorial(input), is(expected));
    }
}