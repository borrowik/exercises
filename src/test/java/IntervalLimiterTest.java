import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class IntervalLimiterTest {

/*
    Things can change very quickly on the markets and we often find that
    we have a need to limit the amount of change that can occur in a given
    interval of time. For example, to help manage risk we may wish to limit
    the total value of stock we trade in any one minute period to $10,000
    worth.
    Your task is to complete the implementation of a class
    called IntervalLimiter that helps enforce such limits. The class constructor
    takes two arguments, limit and interval. The limit is the maximum total
    change that can be applied over a period of interval seconds. You may
    assume that both limit and interval are non-negative integers.
    Apart from its constructor, the class has only one method, apply, which
    takes a value and a time, and returns to its caller an integer between
    zero and value representing the maximum amount of change that may
    be applied at time. In addition, the apply method should record the
    actual change at time for use in subsequent calls. The actual value
    applied at time t should continue to count against the limit up to, but
    excluding, time t + interval. Both value and time will be integers and you
    may assume that time is non-negative and monotonically increases with
    each call to apply. However, value may be negative and your class
    should limit the absolute value of change over the interval.*/

    private List<Long> results = new ArrayList<>();

    @BeforeEach
    void setUp() {
        results.clear();
    }

    @Test
    void intervalLimiterTest() {
        // given
        long limit = 10;
        long interval = 5;
        IntervalLimiter limiter = new IntervalLimiter(limit,interval);


        // when
        results.add(limiter.apply(2,1));
        results.add(limiter.apply(3,2));
        results.add(limiter.apply(7,3));
        results.add(limiter.apply(2,4));
        results.add(limiter.apply(1,5));
        results.add(limiter.apply(3,6));
        results.add(limiter.apply(1,7));

        // then
        assertThat(results.get(0), is(2L));
        assertThat(results.get(1), is(3L));
        assertThat(results.get(2), is(5L));
        assertThat(results.get(3), is(0L));
        assertThat(results.get(4), is(0L));
        assertThat(results.get(5), is(2L));
        assertThat(results.get(6), is(1L));
    }

}