import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NewYearChaosTest {

/*    There are a number of people queued up, and each person wears a sticker indicating their
    initial position in the queue. Initial positions increment by 1 from 1 at the front of the line
    to n at the back. Any person in the queue can bribe the person directly in front of them to
    swap positions. If two people swap positions, they still wear the same sticker denoting
    their original places in line. One person can bribe at most two others. For example,
    if n=8 and person5 bribes person4, the queue will look like this: 1,2,3,5,4,6,7,8.
    Fascinated by this chaotic queue, you decide you must know the minimum number of bribes
    that took place to get the queue into its current state!
    Function Description
    Complete the function minimumBribes in the editor below. It must print an integer representing
    the minimum number of bribes necessary, or Too chaotic if the line configuration is not possible.
    minimumBribes has the following parameter(s):
    q: an array of integers
    Output Format
    Print an integer denoting the minimum number of bribes needed to get the queue into its final state.
    Print Too chaotic if the state is invalid, i.e. it requires a person to have bribed more than 2
    people.*/

    @Test
    void shouldFindTheNoBribeNeeded() {
        // given
        int[] arr = new int[]{1, 2, 3, 4};
        // when
        String result = NewYearChaos.minimumBribes(arr);
        // then
        assertEquals("0", result);
    }

    @Test
    void shouldFindThreeBribes() {
        // given
        int[] arr = new int[]{2, 1, 5, 3, 4};
        // when
        String result = NewYearChaos.minimumBribes(arr);
        // then
        assertEquals("3", result);
    }

    @Test
    void shouldFindChaoticLine() {
        // given
        int[] arr = new int[]{2, 5, 1, 3, 4};
        // when
        String result = NewYearChaos.minimumBribes(arr);
        // then
        assertEquals("Too chaotic", result);
    }
}