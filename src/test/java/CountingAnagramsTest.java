import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountingAnagramsTest {

/*
    Have the function CountingAnagrams(str) take the str parameter and determine how many anagrams
    exist in the string. An anagram is a new word that is produced from rearranging the characters
    in a different word, for example: cars and arcs are anagrams. Your program should determine
    how many anagrams exist in a given string and return the total number. For example:
    if str is "cars are very cool so are arcs and my os" then your program should return 2
    because "cars" and "arcs" form 1 anagram and "so" and "os" form a 2nd anagram. The word "are"
    occurs twice in the string but it isn't an anagram because it is the same word just repeated.
    The string will contain only spaces and lowercase letters, no punctuation, numbers, or uppercase
    letters.
    Hard challenges are worth 15 points and you are not timed for them.
            Examples
    Input: "aa aa odg dog gdo"
    Output: 2
*/

    @Test
    void shouldFindTwoAnagramsCarSo() {
        // given
        String input = "cars are very cool so are arcs and my os";
        // when
        int result = CountingAnagrams.countingAnagrams(input);
        // then
        assertEquals(2, result);
    }

    @Test
    void shouldFindThreeAnagramsDog() {
        // given
        String input = "aa aa odg dog gdo";
        // when
        int result = CountingAnagrams.countingAnagrams(input);
        // then
        assertEquals(2, result);
    }
}