import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

class ArraysLeftRotationTest {

/*    A left rotation operation on an array shifts each of the array's elements  unit to the left.
    For example, if  left rotations are performed on array , then the array would become .
    Given an array  of  integers and a number, , perform  left rotations on the array.
    Return the updated array to be printed as a single line of space-separated integers.*/

    @Test
    void shouldRotateArrayLength() {
        // given
        int left = 4;
        int[] arr = new int[]{1, 2, 3, 4};

        // when
        int[] result = ArraysLeftRotation.rotLeft(arr, left);

        // then
        List<Integer> sequence = Arrays.stream(result).boxed().collect(toList());
        assertThat(sequence, contains(1, 2, 3, 4));
    }

    @Test
    void shouldRotFourElements() {
        // given
        int left = 4;
        int[] arr = new int[]{1, 2, 3, 4, 5};

        // when
        int[] result = ArraysLeftRotation.rotLeft(arr, left);

        // then
        List<Integer> sequence = Arrays.stream(result).boxed().collect(toList());
        assertThat(sequence, contains(5, 1, 2, 3, 4));
    }
}