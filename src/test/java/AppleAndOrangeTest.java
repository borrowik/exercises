import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

class AppleAndOrangeTest {

    /*
    Sam's house has an apple tree and an orange tree that yield an abundance of fruit. His house stands on s where it is
    the start point, and t that is the endpoint. The apple tree is to the left of his house, and the orange tree is to its right.
    You can assume the trees are located on a single point, where the apple tree is at point a, and the orange tree is at point b.
    When a fruit falls from its tree, it lands d units of distance from its tree of origin along the x-axis.
    A negative value of d means the fruit fell d units to the tree's left, and a positive value of d means it falls d
    units to the tree's right.
    Given the value of d for m apples and n oranges, determine how many apples and oranges will fall on Sam's house
    (i.e., in the inclusive range [s,t])?
    For example, Sam's house is between s=7 and t=10. The apple tree is located at a=4 and the orange at b=12. There are
    3 apples and 3 oranges. Apples are thrown [2,3,-4] units distance from a, and oranges [3,-2,-4] units distance.
    Adding each apple distance to the position of the tree, they land at [6,7,0]. Oranges land at [15,10,8].
    One apple and two oranges land in the inclusive range 7-10 so we print:
    1
    2
    */

    @Test
    void shouldGetOneApple() {
        // given
        int s = 7;
        int t = 10;
        int a = 4;
        int b = 12;
        int[] apples = new int[]{2, 3, -4};
        int[] oranges = new int[]{12, 12, 12};

        // when
        int[] result = AppleAndOrange.countApplesAndOranges(s,t,a,b,apples,oranges);

        // then
        assertThat(result.length, is(2));
        assertThat(result[0], is(1));
    }

    @Test
    void shouldGetTwoOranges() {
        // given
        int s = 7;
        int t = 10;
        int a = 4;
        int b = 12;
        int[] apples = new int[]{4, 4, 4};
        int[] oranges = new int[]{3, -2, -4};

        // when
        int[] result = AppleAndOrange.countApplesAndOranges(s,t,a,b,apples,oranges);

        // then
        assertThat(result.length, is(2));
        assertThat(result[1], is(2));
    }

    @Test
    void shouldGetTwoApplesAndThreeOranges() {
        // given
        int s = 7;
        int t = 10;
        int a = 4;
        int b = 12;
        int[] apples = new int[]{2, 3, -4, 4};
        int[] oranges = new int[]{3, -2, -4,-3};

        // when
        int[] result = AppleAndOrange.countApplesAndOranges(s,t,a,b,apples,oranges);

        // then
        assertThat(result.length, is(2));
        assertThat(result[0], is(2));
        assertThat(result[1], is(3));
    }
}