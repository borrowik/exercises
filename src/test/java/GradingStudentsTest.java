import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

class GradingStudentsTest {

/*
    University has the following grading policy:
    Every student receives a grade in the inclusive range from 1 to 100.
    Any grade less than 40 is a failing grade.
    Sam is a professor at the university and likes to round each student's grade according to these rules:
    -If the difference between the grade and the next multiple of 5 is less than , round grade up to the next multiple of 5.
    -If the value of grade is less than 38, no rounding occurs as the result will still be a failing grade.
    For example, grade=84 will be rounded to 85 but grade=29 will not be rounded because the rounding would result in a number that is less than 40.
    Given the initial value of grade for each of Sam's  students, write code to automate the rounding process.
    Function Description:
    Complete the function gradingStudents in the editor below. It should return an integer array consisting of rounded grades.
    gradingStudents has the following parameter(s):
    grades: an array of integers representing grades before rounding*/

    @Test
    void shouldNotRoundLessThen38() {
        // given
        List<Integer> a = Arrays.asList(2);

        // when
        List<Integer> result = GradingStudents.gradingStudents(a);

        // then
        assertThat(result, hasSize(1));
        assertThat(result.get(0), is(2));
    }

    @Test
    void shouldRound84() {
        // given
        List<Integer> a = Arrays.asList(84);

        // when
        List<Integer> result = GradingStudents.gradingStudents(a);

        // then
        assertThat(result, hasSize(1));
        assertThat(result.get(0), is(85));
    }

    @Test
    void shouldNotRoundIfDividesTo5() {
        // given
        List<Integer> a = Arrays.asList(70);

        // when
        List<Integer> result = GradingStudents.gradingStudents(a);

        // then
        assertThat(result, hasSize(1));
        assertThat(result.get(0), is(70));
    }

    @Test
    void shouldGrade80To86Range() {
        // given
        List<Integer> a = Arrays.asList(80,81,82,83,84,85,86);

        // when
        List<Integer> result = GradingStudents.gradingStudents(a);

        // then
        assertThat(result, hasSize(7));
        assertThat(result.get(0), is(80));
        assertThat(result.get(1), is(81));
        assertThat(result.get(2), is(82));
        assertThat(result.get(3), is(85));
        assertThat(result.get(4), is(85));
        assertThat(result.get(5), is(85));
        assertThat(result.get(6), is(86));
    }

}