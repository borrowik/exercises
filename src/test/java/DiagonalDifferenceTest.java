import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.is;

class DiagonalDifferenceTest {

 /*    Given a square matrix, calculate the absolute difference between the sums of its diagonals.
        For example, the square matrix  is shown below:
        1 2 3
        4 5 6
        9 8 9
        The left-to-right diagonal 1+5+9=15 . The right to left diagonal 3+5+9=17 . Their absolute difference is |15-17|=2.
        Complete the diagonalDifference function in the editor below. It must return an integer representing the absolute
        diagonal difference. diagonalDifference takes the following parameter:
            arr: an array of integers.
*/

    @Test
    void shouldBeTwoForSequencedArray() {
        // given
        List<List<Integer>> arr = Arrays.asList(
                Arrays.asList(1, 2, 3),
                Arrays.asList(4, 5, 6),
                Arrays.asList(9, 8, 9)
        );

        // when
        int result = DiagonalDifference.diagonalDifference(arr);

        // then
        assertThat(result, is(2));
    }

    @Test
    void shouldBeFifteenForArrayWithNegativeValue() {
        // given
        List<List<Integer>> arr = Arrays.asList(
                Arrays.asList(11, 2, 4),
                Arrays.asList(4, 5, 6),
                Arrays.asList(10, 8, -12)
        );

        // when
        int result = DiagonalDifference.diagonalDifference(arr);

        // then
        assertThat(result, is(15));
    }
}