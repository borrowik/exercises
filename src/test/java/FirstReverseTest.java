import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class FirstReverseTest {

/*    Have the function FirstReverse(str) take the str parameter being passed and return
    the string in reversed order. For example: if the input string is "Hello World and Coders"
    then your program should return the string sredoC dna dlroW olleH.
    */

    @Test
    void shouldReverseString() {
        // given
        String input = "thisiscool";
        String expected = "loocsisiht";

		// when + then
		assertThat(FirstReverse.firstReverse(input), is(expected));
    }

    @Test
    void shouldReverseSpecialCharacters() {
        // given
        String input = "lettersz!23z";
        String expected = "z32!zsrettel";

        // when + then
        assertThat(FirstReverse.firstReverse(input), is(expected));
    }
}