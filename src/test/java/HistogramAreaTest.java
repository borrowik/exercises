import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HistogramAreaTest {

    /*    Have the function HistogramArea(arr) read the array of non-negative integers stored in arr
    which will represent the heights of bars on a graph (where each bar width is 1),
    and determine the largest area underneath the entire bar graph. For example: if arr
    is [2, 1, 3, 4, 1] then this looks like the following bar graph:
    You can see in the above bar graph that the largest area underneath the graph is covered
    by the x's. The area of that space is equal to 6 because the entire width is 2
    and the maximum height is 3, therefore 2 * 3 = 6. Your program should return 6.
    The array will always contain at least 1 element.
    Examples
    Input: new int[] {6, 3, 1, 4, 12, 4}
    Output: 12 */

    @Test
    void shouldFindBiggestRectangle() {
        // given
        int[] arr = new int[]{2, 2, 2, 2, 2};
        // when
        int result = HistogramArea.histogramArea(arr);
        // then
        assertEquals(10, result);
    }

    @Test
    void shouldFindRectangleOne() {
        // given
        int[] arr = new int[]{2, 1, 3, 4, 1};
        // when
        int result = HistogramArea.histogramArea(arr);
        // then
        assertEquals(6, result);
    }

    @Test
    void shouldFindRectangleTwo() {
        // given
        int[] arr = new int[]{6, 3, 1, 4, 11, 4};
        // when
        int result = HistogramArea.histogramArea(arr);
        // then
        assertEquals(12, result);
    }

    @Test
    void shouldFindRectangleWhereZeroValuePresent() {
        // given
        int[] arr = new int[]{6, 3, 0, 4, 12, 4};
        // when
        int result = HistogramArea.histogramArea(arr);
        // then
        assertEquals(12, result);
    }

}