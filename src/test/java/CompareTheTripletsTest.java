import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

class CompareTheTripletsTest {

    /*    Alice and Bob each created one problem for HackerRank. A reviewer rates the two challenges, awarding points
    on a scale from 1 to 100 for three categories: problem clarity, originality, and difficulty.
    We define the rating for Alice's challenge to be the triplet a = (a[0], a[1], a[2]), and the rating for Bob's
    challenge to be the triplet b = (b[0], b[1], b[2]) .
    Your task is to find their comparison points by comparing a[0] with b[0], a[1] with b[1], and  a[2] with b[2].
        If a[i] > b[i] , then Alice is awarded 1 point.
        If a[i] < b[i], then Bob is awarded 1 point.
        If a[i] = b[i], then neither person receives a point.
    Comparison points is the total points a person earned.
    Given a and b, determine their respective comparison points.
    For example, a=[1,2,3] and b=[3,2,1]. For elements , Bob is awarded a point because a[0] < b[0]. For the equal
     elements b[1] and b[1], no points are earned. Finally, for elements 2, a[2] > b[2] so Alice receives a point.
     Your return array would be [1,1] with Alice's score first and Bob's second..*/

    @Test
    void shouldBeEqual() {
        // given
        List<Integer> a = Arrays.asList(1, 2, 3);
        List<Integer> b = Arrays.asList(3, 2, 1);

        // when
        List<Integer> result = CompareTheTriplets.compareTriplets(a, b);

        // then
        assertThat(result, hasSize(2));
        assertThat(result.get(0), is(1));
        assertThat(result.get(1), is(1));
    }

}