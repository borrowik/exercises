import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PlusMinusTest {

/*    Given an array of integers, calculate the fractions of its elements that are positive, negative, and are zeros.
      Print the decimal value of each fraction on a new line.
      Note: This challenge introduces precision problems. The test cases are scaled to six decimal places, though
      answers with absolute error of up to 10e-4 are acceptable.
      For example, given the array arr= [1,1,0,-1,-1] there are 5 elements, two positive, two negative and one zero.
      Their ratios would be 2/5, 2/5 and 1/5. It should be printed as
        0.400000
        0.400000
        0.200000
      Complete the plusMinus function in the editor below. It should print out the ratio of positive, negative and zero
      items in the array, each on a separate line rounded to six decimals.
 */

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    void restore() {
        System.setOut(originalOut);
    }

    @Test
    void shouldSumFiveValues() {
        // given
        int[] arr = new int[]{1,1,0,-1,-1};

        // when
        PlusMinus.plusMinus(arr);

        // then
        String[] out = outContent.toString().split(System.getProperty("line.separator"));
        assertEquals(out.length, 3);
        assertEquals(out[0], "0,400000");
        assertEquals(out[1], "0,400000");
        assertEquals(out[2], "0,200000");
    }

}