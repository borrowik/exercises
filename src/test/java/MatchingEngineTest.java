import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class MatchingEngineTest {

/*
    In order to test some new trading strategies, you need to build simple trading machine simulator which accepts
    orders to buy or sell a security and indicates when trades occur. At the core of your simulator that consumes "B"
    for bid or "A" for ask orders to buy or sell. For each order exists a prise that is > 1.
    Buy orders are ranked to the price time priority principle:
    Buy are ranked in decreasing order of price
    Sell are ranked in increasing order of price
    System matches:
        -a new buy order with a price lower that the highest ranked sell order, or a new  sell order with a price higher than the highest ranked buy order, is added to the order book.
        -a new buy order with a price at lest as high as the highest ranked sell order in the order book matches that sell order and the sell order is removed from the order book(the buy order is not added to the order book)
        -a new sell order with a price at least low as the highest ranked buy order in the order book matches that buy order and the buy order is removed from the order book(the sell order is not added to the book)
        -if there are no sell orders when a new buy is inserted into matching engine or there are no buy orders when a new sell order is inserted, then the new order is added to the order book.
    Return id of the matched order.
    */

    private List<Long> results = new ArrayList<>();

    @BeforeEach
    void setUp() {
        results.clear();
    }

    @Test
    void matchingEngineTestBuyFirst() {
        // given
        MatchingEngine engine = new MatchingEngine();

        // when
        results.add(engine.insert(new Order(5, "B", 25000)));
        results.add(engine.insert(new Order(6, "B", 24999)));
        results.add(engine.insert(new Order(7, "B", 25000)));
        results.add(engine.insert(new Order(8, "A", 24999)));
        results.add(engine.insert(new Order(9, "A", 24999)));

        // then
        assertThat(results.get(0), is(0L));
        assertThat(results.get(1), is(0L));
        assertThat(results.get(2), is(0L));
        assertThat(results.get(3), is(5L));
        assertThat(results.get(4), is(7L));
    }

}