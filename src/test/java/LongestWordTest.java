import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class LongestWordTest {

/*    Have the function LongestWord(sen) take the sen parameter being passed and return
    the largest word in the string. If there are two or more words that are the same length,
    return the first word from the string with that length. Ignore punctuation and assume sen will not be empty.
    */

    @Test
    void shouldGetLongestWord() {
        // given
        String input = "a beautiful sentence^&!";
        String expected = "beautiful";

		// when + then
		assertThat(LongestWord.longestWord(input), is(expected));
    }

    @Test
    void shouldGetLongestWordDigits() {
        // given
        String input = "123456789 98765432";
        String expected = "123456789";

        // when + then
        assertThat(LongestWord.longestWord(input), is(expected));
    }
}