package bowling;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class BowlingTest {

    @Test
    void shouldStartWithScoreZero() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(0));
    }

    @Test
    void shouldRollSingleTime() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        bowlingCounter.roll(4);
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(4));
    }

    @Test
    void shouldNotAddNegativeNumberOfPins() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        bowlingCounter.roll(-1);
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(0));
    }

    @Test
    void shouldNotAddIllegalNumberOfPins() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        bowlingCounter.roll(11);
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(0));
    }

    @Test
    void shouldScorePinsWithTwoRolls() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        bowlingCounter.roll(4);
        bowlingCounter.roll(4);
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(8));
    }

    @Test
    void shouldScoreWhenAllRollsAreThree() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        for(int i = 0 ; i < 20 ; i++) {
            bowlingCounter.roll(3);
        }
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(60));
    }

    @Test
    void shouldScoreNoMoreThanAvailableRolls() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        for(int i = 0 ; i < 22 ; i++) {
            bowlingCounter.roll(1);
        }
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(20));
    }

    @Test
    void shouldAddExtraSpare() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        bowlingCounter.roll(4);
        bowlingCounter.roll(6);
        bowlingCounter.roll(5);
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(20));
    }

    @Test
    void shouldAddExtraStrike() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        bowlingCounter.roll(10);
        bowlingCounter.roll(2);
        bowlingCounter.roll(2);
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(18));
    }

    @Test
    void shouldAddTwoStrikesInRow() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        bowlingCounter.roll(10);
        bowlingCounter.roll(10);
        bowlingCounter.roll(2);
        bowlingCounter.roll(2);
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(40));
    }

    @Test
    void shouldAddThreeStrikesInRow() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        bowlingCounter.roll(10);
        bowlingCounter.roll(10);
        bowlingCounter.roll(10);
        bowlingCounter.roll(2);
        bowlingCounter.roll(2);
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(70));
    }

    @Test
    void shouldMaximumScoreWithAllStrikes() {
        // given
        BowlingCounter bowlingCounter = new BowlingCounter();

        // when
        for(int i = 0 ; i < 12 ; i++) {
            bowlingCounter.roll(10);
        }
        int score = bowlingCounter.score();

        // then
        assertThat(score, is(300));
    }

}