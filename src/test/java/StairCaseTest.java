import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class StairCaseTest {

        /*  Consider a staircase of size :
           #
          ##
         ###
        ####
        Observe that its base and height are both equal to , and the image is drawn using # symbols and spaces.
        The last line is not preceded by any spaces.
        Write a program that prints a staircase of size .
 */

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    void setUp() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    void restore() {
        System.setOut(originalOut);
    }

    @Test
    void shouldEmptyValues() {
        // given
        int n = 0;

        // when
        StairCase.staircase(n);

        // then
        String[] out = outContent.toString().split(System.getProperty("line.separator"));
        assertEquals( 1, out.length);
        assertEquals("", out[0]);
    }

    @Test
    void shouldPrintSingleStair() {
        // given
        int n = 1;

        // when
        StairCase.staircase(n);

        // then
        String[] out = outContent.toString().split(System.getProperty("line.separator"));
        assertEquals(1, out.length);
        assertEquals("#", out[0]);
    }

    @Test
    void shouldPrintFourStairs() {
        // given
        int n = 4;

        // when
        StairCase.staircase(n);

        // then
        String[] out = outContent.toString().split(System.getProperty("line.separator"));
        assertEquals(out.length, 4);
        assertEquals("   #", out[0]);
        assertEquals("  ##", out[1]);
        assertEquals(" ###", out[2]);
        assertEquals("####", out[3]);
    }
}