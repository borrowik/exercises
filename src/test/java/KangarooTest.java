import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class KangarooTest {

    /*    You are choreographing a circus show with various animals. For one act, you are given two kangaroos on a number
        line ready to jump in the positive direction (i.e, toward positive infinity).
        The first kangaroo starts at location  and moves at a rate of  meters per jump.
        The second kangaroo starts at location  and moves at a rate of  meters per jump.
        You have to figure out a way to get both kangaroos at the same location at the same time as part of the show.
        If it is possible, return YES, otherwise return NO.

        For example, kangaroo 1 starts at x1=2 with a jump distance 1 and kangaroo 2 starts at x2=1 with a jump distance of 2.
        After one jump, they are both at 3, (1+1*2 = 3, 2+1*1=3), so our answer is YES.
     */

    private static final String Yes = "YES";
    private static final String No = "NO";

    @Test
    void shouldBePositiveAtTheSameStart() {
        // given
        int x1 = 2;
        int v1 = 5;
        int x2 = 2;
        int v2 = 7;

        // when
        String result = Kangaroo.kangaroo(x1, v1, x2, v2);

        // then
        assertThat(result.equals(Yes), is(true));
    }

    @Test
    void shouldBePositiveAt3() {
        // given
        int x1 = 2;
        int v1 = 1;
        int x2 = 1;
        int v2 = 2;

        // when
        String result = Kangaroo.kangaroo(x1, v1, x2, v2);

        // then
        assertThat(result.equals(Yes), is(true));
    }

    @Test
    void shouldBeNegativeWhenNotCrossed() {
        // given
        int x1 = 2;
        int v1 = 1;
        int x2 = 3;
        int v2 = 2;

        // when
        String result = Kangaroo.kangaroo(x1, v1, x2, v2);

        // then
        assertThat(result.equals(No), is(true));
    }

    @Test
    void shouldBeNegativeWhenCrossed() {
        // given
        int x1 = 4;
        int v1 = 1;
        int x2 = 3;
        int v2 = 4;

        // when
        String result = Kangaroo.kangaroo(x1, v1, x2, v2);

        // then
        assertThat(result.equals(No), is(true));
    }

    @Test
    void shouldBePositive() {
        // given
        int x1 = 4523;
        int v1 = 8092;
        int x2 = 9419;
        int v2 = 8076;

        // when
        String result = Kangaroo.kangaroo(x1, v1, x2, v2);

        // then
        assertThat(result.equals(Yes), is(true));
    }
}