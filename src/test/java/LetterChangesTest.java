import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class LetterChangesTest {

/*    Have the function LetterChanges(str) take the str parameter being passed and modify it using
    the following algorithm. Replace every letter in the string with the letter following it in
    the alphabet (ie. c becomes d, z becomes a). Then capitalize every vowel in this
    new string (a, e, i, o, u) and finally return this modified string.
    */

    @Test
    void shouldGetLetterChangedHelloWorld() {
        // given
        String input = "hello world";
        String expected = "Ifmmp xpsmE";

        // when + then
        assertThat(LetterChanges.letterChanges(input), is(expected));
    }

    @Test
    void shouldGetLetterChangedWithSpecial() {
        // given
        String input = "beautiful^";
        String expected = "cfbvUjgvm^";

		// when + then
		assertThat(LetterChanges.letterChanges(input), is(expected));
    }
}