import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MiniMaxSumTest {
/*
   Given five positive integers, find the minimum and maximum values that can be calculated by summing exactly four
   of the five integers. Then print the respective minimum and maximum values as a single line of two space-separated long integers.
   For example, arr[1,3,5,7,9]. Our minimum sum is 1+3+5+7=16 and our maximum sum is 3+5+7+9=24. We would print
   16 24
*/

    @Test
    void shouldFirstPair() {
        // given
        int[] arr = new int[]{1, 3, 5, 7, 9};
        // when
        String result = MiniMaxSum.miniMaxSum(arr);
        // then
        assertEquals("16 24", result);
    }

    @Test
    void shouldSecondPair() {
        // given
        int[] arr = new int[]{793810624, 895642170, 685903712, 623789054, 468592370};

        // when
        String result = MiniMaxSum.miniMaxSum(arr);

        // then
        assertEquals("2572095760 2999145560", result);
    }

}