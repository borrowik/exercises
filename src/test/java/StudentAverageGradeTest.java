import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentAverageGradeTest {

    @Test
    void shouldReturnEmptyWhenNoStudents() {
        // given
        String[][] input = new String[][]{};
        // when
        float result = StudentAverageGrade.calculateHighestAverage(input);
        // then
        assertEquals(0, result);
    }

    @Test
    void shouldCalculateAverageFromOneGrade() {
        // given
        String[][] input = new String[][]{
                {"Mat","20"}
        };
        // when
        float result = StudentAverageGrade.calculateHighestAverage(input);
        // then
        assertEquals(20, result);
    }

    @Test
    void shouldCalculateAverageFromTwoGrades() {
        // given
        String[][] input = new String[][]{
                {"Mat","20"},
                {"Mat","80"}
        };
        // when
        float result = StudentAverageGrade.calculateHighestAverage(input);
        // then
        assertEquals(50, result);
    }

    @Test
    void shouldFindMaximumAverageStudents() {
        // given
        String[][] input = new String[][]{
                {"Mat","20"},
                {"Arek", "49"},
                {"Arek", "49"},
                {"Mat", "50"}
        };
        // when
        float result = StudentAverageGrade.calculateHighestAverage(input);
        // then
        assertEquals(49, result);
    }

    @Test
    void shouldFindMaximumAverageStudentsWithNegativeValues() {
        // given
        String[][] input = new String[][]{
                {"Mat","-20"},
                {"Arek", "-49"},
                {"Arek", "-49"},
                {"Mat", "-50"}
        };
        // when
        float result = StudentAverageGrade.calculateHighestAverage(input);
        // then
        assertEquals(-35, result);
    }

}