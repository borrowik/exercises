import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class RepeatedStringTest {

/*    Lilah has a string s, , of lowercase English letters that she repeated infinitely many times.
      Given an integer n, , find and print the number of letter a's in the first  letters of Lilah's infinite string.
      For example, if the string s ='abcac' and n=10, the substring we consider is 'abcacabcac', the first  characters
      of her infinite string. There are  occurrences 4 of a in the substring.
      Function Description:Complete the repeatedString function in the editor below.
      It should return an integer representing the number of occurrences of a in the prefix
      of length n in the infinitely repeating string.
    */

    @Test
    void shouldGetRepeatedString() {
        // given
        String input = "aba";
        int n = 10;
        long expected = 7;

		// when + then
		assertThat(RepeatedString.repeatedString(input, n), is(expected));
    }

    @Test
    void shouldGetRepeatedStringWithLongValues() {
        // given
        String input = "a";
        long n = 1000000000000L;
        long expected = 1000000000000L;

        // when + then
        assertThat(RepeatedString.repeatedString(input, n), is(expected));
    }
}