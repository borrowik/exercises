class MiniMaxSum {

    static String miniMaxSum(int[] arr) {
        int min = Integer.MAX_VALUE;
        int max = 0;
        long sum = 0;
        for (int value : arr) {
            sum += value;
            if (value < min) {
                min = value;
            }
            if (value > max) {
                max = value;
            }
        }
        return (sum - max) + " " + (sum - min);
    }

}
