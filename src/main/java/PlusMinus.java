import java.util.Arrays;

class PlusMinus {

    static void plusMinus(int[] arr) {
        int[] result = new int[]{0, 0, 0};
        for (int value : arr) {
            if (value == 0) {
                result[2]++;
                continue;
            }
            if (value > 0) {
                result[0]++;
            } else {
                result[1]++;
            }
        }
        Arrays.stream(result)
                .boxed()
                .map(count -> (double) count / arr.length)
                .map(value -> String.format("%.6f", value))
                .forEach(System.out::println);
    }

}
