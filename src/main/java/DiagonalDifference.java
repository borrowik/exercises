import java.util.List;

class DiagonalDifference {

    static int diagonalDifference(List<List<Integer>> arr) {
        int last = arr.size()-1;
        int result = 0;
        for(int i = 0 ; i <= last; i++) {
            result += arr.get(i).get(i) - arr.get(last-i).get(i);
        }
        return Math.abs(result);
    }

}
