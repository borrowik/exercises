import java.util.List;
import java.util.stream.Collectors;

class GradingStudents {

    static List<Integer> gradingStudents(List<Integer> grades) {
        return  grades.stream().map(GradingStudents::rounding).collect(Collectors.toList());
    }

    private static Integer rounding(Integer grade) {
        if(grade < 38) return grade;
        int mod = grade%5;
        return mod > 2 ? grade + 5-mod : grade;
    }

}
