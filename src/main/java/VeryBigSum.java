import java.math.BigInteger;
import java.util.Arrays;

class VeryBigSum {

    static long aVeryBigSum(long[] arr) {
        return Arrays.stream(arr)
                .mapToObj(BigInteger::valueOf)
                .reduce(BigInteger.valueOf(0L), BigInteger::add, BigInteger::add)
                .longValue();
    }
}
