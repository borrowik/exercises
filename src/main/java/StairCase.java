import java.util.stream.IntStream;

class StairCase {

    static void staircase(int n) {
        IntStream.rangeClosed(1, n)
                .mapToObj(level -> " ".repeat(n-level) + "#".repeat(level))
                .forEach(System.out::println);
    }

}
