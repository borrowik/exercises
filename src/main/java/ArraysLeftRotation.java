import java.util.Stack;

public class ArraysLeftRotation {

    static int[] rotLeft(int[] a, int left) {
        int[] result = new int[a.length];
        int shifts = left%a.length;
        Stack<Integer> s = new Stack<>();
        int i;
        for (i = 0; i < a.length; i++) {
            if (i < shifts) {
                s.push(a[i]);
            } else {
                result[i-shifts] = a[i];
            }
        }
        while (!s.isEmpty()) {
            result[--i] = s.pop();
        }
        return result;
    }

}
