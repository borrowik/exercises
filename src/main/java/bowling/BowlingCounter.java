package bowling;

import java.util.*;

class BowlingCounter {

    private static final int MAXIMUM_PINS = 10;

    private Integer totalScore;
    private Integer rollCount;
    private int availableRoles;
    private final List<Frame> scores;
    private final Set<Extra> extraFrameType;

    BowlingCounter() {
        this.totalScore = 0;
        this.rollCount = 0;
        this.availableRoles = 20;
        this.scores = new ArrayList<>();
        this.extraFrameType = new HashSet<>();
    }

    int score() {
        return this.totalScore;
    }

    void roll(int pins) {
        if (!isRollPossible(pins)) {
            return;
        }
        int frameNumber = rollCount++ / 2;
        boolean isFirstRoll = isFirstRoll();
        Frame currentFrame = addToScores(pins, frameNumber, isFirstRoll);
        totalScore += pins;
        if (frameNumber >= 1 && !extraFrameType.isEmpty()) {
            totalScore += calculateExtra(currentFrame, frameNumber, isFirstRoll);
        }
        boolean currentStrike = currentFrame.isStrike();
        if (currentStrike || currentFrame.first + currentFrame.second == MAXIMUM_PINS ) {
            extraFrameType.add(currentStrike ? Extra.STRIKE : Extra.SPARE);
            addEndingFrames(frameNumber);
        }
        if (currentStrike) {
            rollCount++;
        }
    }

    private void addEndingFrames(int frameNumber) {
        if(frameNumber == 9) {
            availableRoles+=2;
        }
    }

    private Integer calculateExtra(Frame currentFrame, int frameNumber, boolean isFirstRoll) {
        int extraValue = 0;
        if (extraFrameType.contains(Extra.SPARE)) {
            extraValue += currentFrame.first;
            extraFrameType.remove(Extra.SPARE);
        }
        if (extraFrameType.contains(Extra.STRIKE)) {
            extraValue += getStrikeExtra(currentFrame, frameNumber, isFirstRoll);
        }
        return extraValue;
    }

    private int getStrikeExtra(Frame currentFrame, int frameNumber, boolean isFirstRoll) {
        int extra = 0;
        if (isFirstRoll) {
            extra += currentFrame.first;
            if (frameNumber > 1 && scores.get(frameNumber - 2).isStrike()) {
                extra += currentFrame.first;
            }
        } else {
            extraFrameType.remove(Extra.STRIKE);
            extra += currentFrame.second;
        }

        return extra;
    }

    private Frame addToScores(int pins, int frameNumber, boolean isFirstRoll) {
        Frame frame;
        if (isFirstRoll) {
            frame = new Frame(pins);
            scores.add(frame);
        } else {
            frame = scores.get(frameNumber);
            frame.setSecond(pins);
        }
        return frame;
    }

    private boolean isRollPossible(int pins) {
        return pins > 0 && pins < 11 && availableRoles - rollCount > 0;
    }

    private boolean isFirstRoll() {
        return rollCount % 2 != 0;
    }

    private static class Frame {
        private int first;
        private int second;

        private Frame(int first) {
            this.first = first;
            this.second = 0;
        }

        private void setSecond(int second) {
            this.second = second;
        }

        private boolean isStrike() {
            return first == MAXIMUM_PINS;
        }

    }

    private enum Extra {
        SPARE,
        STRIKE
    }
}
