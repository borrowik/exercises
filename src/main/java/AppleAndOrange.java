import java.util.Arrays;


class AppleAndOrange {

    static int[] countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
        long resultA = countFruitsInHouse(a, s, t, apples);
        for (int i = 0; i < oranges.length; i++) {
            oranges[i] = -oranges[i];
        }
        long resultO = countFruitsInHouse(-b, -t, -s, oranges);
        return new int[]{(int) resultA,(int) resultO};
    }
    private static long countFruitsInHouse(int tree, int houseBegin, int houseEnds, int[] fruits){
        return Arrays.stream(fruits).boxed()
                .map(f -> tree + f)
                .filter(l -> l >= houseBegin)
                .filter(l -> l <= houseEnds)
                .count();
    }
}
