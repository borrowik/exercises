import javax.naming.SizeLimitExceededException;

class Kangaroo {

    static String kangaroo(int x1, int v1, int x2, int v2) {
        Animal first = new Animal(x1, v1);
        Animal second = new Animal(x2, v2);
        if (x1 > x2) {
            first = new Animal(x2, v2);
            second = new Animal(x1, v1);
        }
        while (first.getPosition() <= second.getPosition()) {
            if (first.getPosition() == second.getPosition()) return "YES";
            try {
                first.jump();
                second.jump();
            } catch (ArithmeticException e) {
                return "NO";
            }
        }
        return "NO";
    }

    private static class Animal {
        private int position;
        private int jumpDistance;

        private Animal(int position, int jumpDistance) {
            this.position = position;
            this.jumpDistance = jumpDistance;
        }

        private int getPosition() {
            return position;
        }

        private void jump() throws ArithmeticException {
            this.position = Math.addExact(position , jumpDistance);
        }
    }
}
