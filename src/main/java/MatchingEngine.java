import java.util.Comparator;
import java.util.PriorityQueue;

class MatchingEngine {

    private final PriorityQueue<Order> rankedBuyingBook;
    private final PriorityQueue<Order> rankedSellingBook;

    MatchingEngine() {
        rankedBuyingBook = new PriorityQueue<>(Comparator.comparing(Order::getPrice).reversed());
        rankedSellingBook = new PriorityQueue<>(Comparator.comparing(Order::getPrice));
    }

    long insert(Order order) {
        PriorityQueue<Order> originQueue = rankedBuyingBook;
        PriorityQueue<Order> matchingQueue = rankedSellingBook;
        if(Order.Type.SELL.equals(order.getType())) {
            originQueue = rankedSellingBook;
            matchingQueue = rankedBuyingBook;
        }
        Order bestMatching = matchingQueue.stream()
                .filter(o -> order.getPrice() < o.getPrice())
                .findFirst()
                .orElse(null);
        if(bestMatching == null) {
            originQueue.add(order);
            return 0L;
        } else {
            matchingQueue.remove(bestMatching);
            return bestMatching.getId();
        }
    }

}

final class Order {

    private final long id;
    private final Type type;
    private final long price;

    Order(long id, String type, long price) {
        this.id = id;
        this.type = "B".equals(type) ? Type.BUY : Type.SELL;
        this.price = price;
    }

    protected enum Type {
        BUY,
        SELL
    }

    long getId() {
        return id;
    }

    Type getType() {
        return type;
    }

    long getPrice() {
        return price;
    }
}
