import java.util.Arrays;
import java.util.List;

class CompareTheTriplets {


    static List<Integer>  compareTriplets(List<Integer> a, List<Integer> b) {
        int countA = 0;
        int countB = 0;
        for(int i = 0; i < a.size(); i++){
            int av= a.get(i);
            int bv= b.get(i);
            if(av == bv){
                continue;
            }
            if (av > bv) {
                countA++;
            } else {
                countB++;
            }
        }
        return Arrays.asList(countA,countB);
    }
}
