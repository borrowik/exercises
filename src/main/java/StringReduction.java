import java.util.HashMap;

public class StringReduction {

    private static final HashMap<String, String> replaceable;

    static {
        replaceable = new HashMap<>();
        replaceable.put("ab", "c");
        replaceable.put("ac", "b");
        replaceable.put("ba", "c");
        replaceable.put("bc", "a");
        replaceable.put("ca", "b");
        replaceable.put("cb", "a");
    }

    public static String stringReduction(String s) {
        for (String r : replaceable.keySet()) {
            if(s.contains(r)){
                return stringReduction(s.replaceFirst(r, replaceable.get(r)));
            }
        }
        return s;
    }
}
