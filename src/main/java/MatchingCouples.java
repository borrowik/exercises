import javafx.css.Match;

import java.util.stream.LongStream;

public class MatchingCouples {

    static long matchingCouples(int[] arr) {
        int p = arr[2]/2;
        long b = factorial(arr[0]) / (factorial(p) * factorial(arr[0] -p ));
        long g = factorial(arr[1]) / (factorial(p) * factorial(arr[1] -p ));
        return b * g * p;
    }

    private static long factorial(int n) {
        return LongStream.rangeClosed(1, n)
                .reduce(1, (long x, long y) -> x * y);
    }
}
