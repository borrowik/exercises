public class LongestWord {

    static String longestWord(String sen) {
        StringBuilder stringBuilder = new StringBuilder(sen);
        for (int i = 0 ; i < sen.length(); i++) {
            char c = stringBuilder.charAt(i);
            if(!(Character.isAlphabetic(c) || Character.isDigit(c))){
                stringBuilder.setCharAt(i, ' ');
            }
        }
        String[] words = stringBuilder.toString().split(" ");
        String longest = "";
        for (String word : words) {
            if (longest.length() < word.length()) {
                longest = word;
            }
        }
        return longest;
    }
}
