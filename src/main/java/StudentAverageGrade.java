import java.util.HashMap;
import java.util.Map;

class StudentAverageGrade {

    static Integer calculateHighestAverage(String[][] input){
        Map<String, Scores> nameToScores = new HashMap<>();
        for (String[] s: input){
            String name = s[0];
            Scores scores = nameToScores.getOrDefault(name, new Scores());
            scores.addScore(Integer.parseInt(s[1]));
            nameToScores.put(name,scores);
        }
        return nameToScores.values().stream().map(Scores::getAverage).max(Integer::compareTo).orElse(0);
    }

    private static class Scores {
        int summary;
        int count;

        private Scores() {
            this.summary = 0;
            this.count = 0;
        }

        private void addScore(Integer score) {
            this.summary +=score;
            this.count++;
        }

        private Integer getAverage(){
            return summary/count;
        }
    }
}
