public class FirstReverse {

    static String firstReverse(String str) {
        return new String(new StringBuilder().append(str).reverse());
    }

}
