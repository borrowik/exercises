public class LetterChanges {

    static String letterChanges(String str) {
        char[] change = str.toCharArray();
        char[] replace = {'b', 'c', 'd' , 'e', 'f', 'g', 'h', 'i', 'j', 'k','l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u','v', 'w', 'x', 'y', 'z', 'a' };
        char[] upper = {'a', 'e', 'i', 'o', 'u'};
        for (int i = 0 ; i < change.length ; i++) {
            if(Character.isAlphabetic(change[i])){
                change[i] = replace[change[i] - 97];
            }
        }
        String replaced = new String(change);
        for (char c : upper) {
            replaced = replaced.replace(c, Character.toUpperCase(c));
        }
        return replaced;
    }

}
