public class RepeatedString {

    static long repeatedString(String s, long n) {
        long a = countA(s);
        long times = n / s.length();
        return a * times  + countA(s.substring(0, (int) (n - times * s.length())));
    }

    private static long countA(String s) {
        return s.codePoints().filter(c-> c == 'a').count();
    }

}
