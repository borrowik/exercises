import java.util.LinkedList;
import java.util.Queue;

class IntervalLimiter {

    private final long limit;
    private final long interval;
    private final Queue<Applied> appliedQueue;

    private long availableAmount;

    IntervalLimiter(long limit, long interval) {
        this.limit = limit;
        this.interval = interval;
        this.appliedQueue = new LinkedList<>();
        this.availableAmount = limit;
    }

    /*
     * Return the limited value.
     */
    long apply(long value, long monotonic_time) {
        refill(monotonic_time);
        long absValue = Math.abs(value);
        long result;
        if (availableAmount > absValue) {
            availableAmount -= absValue;
            result = value;
        } else {
            result = availableAmount;
            availableAmount = 0;
        }
        if (result > 0) {
            appliedQueue.add(new Applied(absValue, monotonic_time));
        }
        return result;
    }

    private void refill(long monotonic_time) {
        if (appliedQueue.isEmpty()) {
            return;
        }
        long amountToRefill = 0;
        Applied applied = appliedQueue.peek();
        while (applied != null && isOutdated(applied, monotonic_time)) {
            amountToRefill += applied.getValue();
            appliedQueue.poll();
            applied = appliedQueue.peek();
        }
        availableAmount = Math.min(availableAmount + amountToRefill, limit);
    }

    private boolean isOutdated(Applied applied, long monotonic_time) {
        return monotonic_time - applied.getTimestamp() >= interval;
    }

    private static class Applied {

        private final long value;
        private final long timestamp;

        Applied(long value, long timestamp) {
            this.value = value;
            this.timestamp = timestamp;
        }

        long getValue() {
            return value;
        }

        long getTimestamp() {
            return timestamp;
        }
    }
}
