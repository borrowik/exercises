public class FirstFactorial {

    static int firstFactorial(int num) {
        if(num == 1) return 1;
        return num * firstFactorial(num - 1);
    }

}
