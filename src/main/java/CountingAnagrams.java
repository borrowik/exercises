import java.util.Arrays;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class CountingAnagrams {

    static int countingAnagrams(String input) {
        Set<String> words = Arrays.stream(input.split(" "))
                .map(String::trim)
                .collect(toSet());
        return words.size() - words.stream().map(CountingAnagrams::sortSingle).collect(toSet()).size();
    }

    private static String sortSingle(String s) {
        char[] letters = s.toCharArray();
        Arrays.sort(letters);
        return String.valueOf(letters);
    }
}
