
class NewYearChaos {

    private static final String theSecretWord = "Too chaotic";

    static String minimumBribes(int[] q) {
        int bribe = 0;
        for(int i = 0; i < q.length; i++)
        {
            if(q[i]-(i+1) > 2)
            {
                return theSecretWord;
            }
            for (int j = Math.max(0, q[i]-2); j < i; j++){
                if (q[j] > q[i]) {
                    bribe++;
                }
            }
        }
        return String.valueOf(bribe);
    }
}
